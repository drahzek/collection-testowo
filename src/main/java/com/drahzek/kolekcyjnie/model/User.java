package com.drahzek.kolekcyjnie.model;

public class User implements Comparable<User> {
    private String login;
    private String name;
    private String surname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public User(String name, String surname, String login) {
        this.name = name;
        this.surname = surname;
        this.login = login;
    }

    @Override
    public String toString() {
        return this.getName() + "\n" + this.getSurname() + "\n" + this.getLogin();
    }

    @Override
    public int compareTo(User user) {

        int ret = surname.compareTo(user.getSurname());
        if (ret == 0) {
            ret = name.compareTo(user.getLogin());
        }
        return -ret;

    }
}
