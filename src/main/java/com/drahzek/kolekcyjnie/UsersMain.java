package com.drahzek.kolekcyjnie;

import com.drahzek.kolekcyjnie.model.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class UsersMain {
    public static void main(String[] args) {

        List<User> users;

        System.out.println("Lista użytkowników");

        users = load("lista_uzytkownikow.txt");

        printUserList(users);

        Set<User> userSet = new HashSet<>();
        userSet.addAll(users);

        System.out.println("\n Sortujemy! \n");
        SortedSet<User> sortedUsers = new TreeSet<>();
        sortedUsers.addAll(users);
        printUserList(sortedUsers);
    }

    private static List<User> load(String File) {

        try (BufferedReader br = new BufferedReader(new FileReader(File))) {

            String textSize = br.readLine();
            int size = Integer.parseInt(textSize);

            List<User> users = new ArrayList<>();//Use interface for variable type. No size required.

            for (int i = 0; i < size; i++) {
                String name = br.readLine();
                String surname = br.readLine();
                String login = br.readLine();

                users.add(new User(name, surname, login));

            }
            return users;
        } catch (IOException ex) {
            System.err.println(ex);
            return new ArrayList<>();
        }
    }

    private static void printUserList(Collection userSet) {
        for (Object user : userSet) {
            System.out.println(user);
        }
    }
}
